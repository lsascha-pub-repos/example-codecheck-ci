<?php

$finder = PhpCsFixer\Finder::create()
    ->in(__DIR__ . '/DistributionPackages/*')
;

$config = new PhpCsFixer\Config();

return $config->setRules([
        '@PSR12' => true,
        'array_syntax' => ['syntax' => 'short'],
    ])
    ->setFinder($finder)
;
