#!/bin/bash

set -e

IS_MERGE_REQUEST=false
FUNCTIONNAME=lint

CODE_SUBDIRECTORY=DistributionPackages

while getopts ":c:b:t:m:f:" opt; do
  case $opt in
    c) COMMIT_SHA="$OPTARG"
    ;;
    b) SOURCE_BRANCH_NAME="$OPTARG"
    ;;
    t) TARGET_BRANCH_NAME="$OPTARG"
    ;;
    m) IS_MERGE_REQUEST="$OPTARG"
    ;;
    f) FUNCTIONNAME="$OPTARG"
    ;;
    \?) echo -e "\nInvalid option -$OPTARG" >&2
    ;;
  esac
done

PROJECT=$(php -r "echo dirname(dirname(realpath('$0')));")
EXEC_PATH="${PROJECT}/vendor/bin"
#STAGED_FILES_CMD=`git diff-tree --no-commit-id --name-only -r $CI_COMMIT_SHA | grep \\\\.php$`
if [ "${IS_MERGE_REQUEST}" = "false" ]; then
  # use commit sha if it is a regular commit
  #STAGED_FILES_CMD=$(git diff-tree --no-commit-id --name-only -r "$COMMIT_SHA" | grep \.php$)
  STAGED_FILES_CMD=$(git diff HEAD~1 --name-only --diff-filter=ACMR | grep \.php$ || true)
else
  # use branch name if it is a merge request
  printf "\nis comparing branches...\n"
  STAGED_FILES_CMD=$(git diff --name-only --diff-filter=ACMR "${TARGET_BRANCH_NAME}".."${SOURCE_BRANCH_NAME}" | grep \.php$ || true)
fi

PHP_CSFIXER_CONFIG=".php-cs-fixer.dist.php"
PHPSTAN_CONFIG="phpstan.deploy.neon"
PSALM_CONFIG="psalm.xml"
PHPCODESNIFFER_CONFIG="phpcs.xml"

# Determine if a file list is passed
if [ "$#" -eq 1 ]
then
    oIFS=$IFS
    IFS='
    '
    SFILES="$1"
    IFS=$oIFS
fi
SFILES=${SFILES:-$STAGED_FILES_CMD}

ERROR_COUNT=0


phpLint() {
    TMP_ERROR=0
    printf "\nChecking PHP Lint...\n"
    for FILE in $SFILES
    do
        php -l -d display_errors=0 "$PROJECT"/"$FILE"
        TMP_ERROR=$?
        if [ $TMP_ERROR != 0 ]
        then
            (( ERROR_COUNT += 1 ))
        fi
    done
}

csFixer() {
    TMP_ERROR=0
    if [ -f "$PHP_CSFIXER_CONFIG" ]; then
        printf "\nRunning CS Fixer Check...\n"
        filecount=0
        for FILE in $SFILES
        do
            "$EXEC_PATH"/php-cs-fixer fix --config=$PHP_CSFIXER_CONFIG -v --dry-run --stop-on-violation --using-cache=no "$PROJECT"/"$FILE"
            TMP_ERROR=$?
            if [ $TMP_ERROR != 0 ]
            then
                (( ERROR_COUNT += 1 ))
            fi

            if [ "${IS_MERGE_REQUEST}" = "true" ]; then
              printf "\nGenerating Report files...\n"
              "$EXEC_PATH"/php-cs-fixer fix --config=$PHP_CSFIXER_CONFIG -v --dry-run --stop-on-violation --using-cache=no "$PROJECT"/"$FILE" --format=gitlab > php-cs-fixer-report-${filecount}.json
            fi
            ((filecount=filecount+1))
        done
    else
        printf "\nNo %s found. skipping CS Fixer Check ...\n" "$PHP_CSFIXER_CONFIG"
    fi
}

codeSniffer() {
    TMP_ERROR=0
    if [ -f "$PHPCODESNIFFER_CONFIG" ]; then
        printf "\nRunning PHP CodeSniffer...\n"

        "$EXEC_PATH"/phpcs --colors --standard=$PHPCODESNIFFER_CONFIG $CODE_SUBDIRECTORY || TMP_ERROR=$?

        # 2 = Error, 1 = Warning
        if [ $TMP_ERROR = 2 ]
        then
            (( ERROR_COUNT += 1 ))
        fi

        if [ "${IS_MERGE_REQUEST}" = "true" ]; then
          printf "\nGenerating Report file...\n"
          "$EXEC_PATH"/phpcs --colors --standard=$PHPCODESNIFFER_CONFIG --report=junit $CODE_SUBDIRECTORY > phpcs-junit-report.xml || true
        fi

    else
        printf "\nNo %s found. skipping PHP CodeSniffer ...\n" "$PHPCODESNIFFER_CONFIG"
    fi
}

phpStan() {
    if [ "$SFILES" != "" ]; then
        if [ -f "$PHPSTAN_CONFIG" ]; then
            printf "\nRunning PHPStan...\n"
            # shellcheck disable=SC2086
            test=$("$EXEC_PATH"/phpstan analyse --no-progress --error-format=raw --configuration=$PHPSTAN_CONFIG ${SFILES}) || TMP_ERROR=$?

            if [ "$test" = "" ]
            then
                echo 'passed'
            else
                echo "$test"
                (( ERROR_COUNT += 1 ))
            fi

            if [ "${IS_MERGE_REQUEST}" = "true" ]; then
              printf "\nGenerating Report file...\n"
              # shellcheck disable=SC2086
              "$EXEC_PATH"/phpstan analyse --no-progress --error-format=gitlab --configuration=$PHPSTAN_CONFIG ${SFILES} > phpstan-report.json || true
            fi

        else
            printf "\nNo %s found. skipping PHPStan ...\n" "$PHPSTAN_CONFIG"
        fi
    fi
}

psalm() {
    if [ "$SFILES" != "" ]; then
        if [ -f "$PSALM_CONFIG" ]; then
            printf "\nRunning Psalm...\n"
            # more than 1 thread requires php pcntl extension
            # shellcheck disable=SC2086
            test=$("$EXEC_PATH"/psalm --config=$PSALM_CONFIG --show-info=false --no-progress --report-show-info=false --output-format=console --threads=1 ${SFILES}) || TMP_ERROR=$?

            if [[ $test == *"No errors found!"* ]];
            then
                echo 'passed'
            else
                echo "$test"
                (( ERROR_COUNT += 1 ))
            fi

            if [ "${IS_MERGE_REQUEST}" = "true" ]; then
              printf "\nGenerating Report file...\n"
              # shellcheck disable=SC2086
              "$EXEC_PATH"/psalm --config=$PSALM_CONFIG --show-info=false --no-progress --report-show-info=false --output-format=junit --threads=1 ${SFILES} > psalm-report.junit.xml || true
            fi

        else
            printf "\nNo %s found. skipping Psalm ...\n" "$PSALM_CONFIG"
        fi
    fi
}

phpunit() {
    printf "\nRunning PHP Unit tests...\n"

    "$EXEC_PATH"/phpunit $CODE_SUBDIRECTORY --log-junit phpunit-junit-report.xml || TMP_ERROR=$?

    if [ "$TMP_ERROR" != "" ]
    then
        (( ERROR_COUNT += 1 ))
    fi
}

case $FUNCTIONNAME in
  phpLint) phpLint
  ;;
  codeSniffer) codeSniffer
  ;;
  csFixer) csFixer
  ;;
  phpStan) phpStan
  ;;
  psalm) psalm
  ;;
  phpunit) phpunit
  ;;
esac

exit $ERROR_COUNT
