#!/bin/bash
# fetch current composer bin directory
composerbin="$(composer config bin-dir)"

# install codecheck packages
composer require "phpstan/phpstan" --dev
composer require "squizlabs/php_codesniffer" --dev
composer require "friendsofphp/php-cs-fixer" --dev
composer require "vimeo/psalm" --dev
composer require "phpunit/phpunit" --dev

# install phpinsights as namespaced package to avoid conflicts
composer require "bamarni/composer-bin-plugin" --dev
composer bin phpinsights require nunomaduro/phpinsights

echo "----------------------------------------------------------------"
codepath="Packages"
read -e -i "$codepath" -p "Enter relative path to directory containing your custom packages: " input
codepath="${input:-$codepath}"

# add scripts for easier codecheck and fixing
composer config scripts.phpstan "${composerbin}/phpstan analyse --ansi ${codepath}"
composer config scripts.psalm "${composerbin}/psalm --show-info=false"
composer config scripts.phpcs "${composerbin}/phpcs --standard=phpcs.xml --colors ${codepath}"
composer config scripts.phpcbf "${composerbin}/phpcbf --standard=phpcs.xml --colors ${codepath}"
composer config scripts.phpcsfixer "${composerbin}/php-cs-fixer fix --ansi -v"

composer config scripts.insights "${composerbin}/phpinsights analyse ${codepath}"

composer config scripts.phpunit "${composerbin}/phpunit ${codepath}"

composer config scripts.check "@phpstan" "@psalm"
composer config scripts.lint "@phpcsfixer --dry-run" "@phpcs"
composer config scripts.checkall "@check" "@lint"
composer config scripts.fix "@phpcsfixer" "@phpcbf"

# change Package directory in config files in which codechecks are executed
sed -i -e "s/DistributionPackages/${codepath}/g" phpstan.neon.dist
sed -i -e "s/DistributionPackages/${codepath}/g" psalm.xml
sed -i -e "s/DistributionPackages/${codepath}/g" .php-cs-fixer.dist.php
sed -i -e "s/DistributionPackages/${codepath}/g" _deploy/ci-codecheck.sh
sed -i -e "s/DistributionPackages/${codepath}/g" _develop/bin/pre-commit

# change composer vendor bin directory
sed -i -e "s,vendor/bin,${composerbin},g" _develop/bin/pre-commit
sed -i -e "s,vendor/bin,${composerbin},g" _deploy/ci-codecheck.sh

# setup git pre-commit hook for codechecks
echo "Setup git pre-commit hook?"
echo "(will rename old pre-commit hook as __bak_pre-commit)"
read -p "[y/n]" -n 1 -r
echo
if [[ $REPLY =~ ^[YyJj]$ ]]
then
    mv .git/hooks/pre-commit .git/hooks/__bak_pre-commit
    ln -s ../../_develop/bin/pre-commit .git/hooks/pre-commit
    chmod +x _develop/bin/pre-commit
fi
