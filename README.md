# Example-CodeCheck-CI

Example Project for PHP Code-checks, Analyse etc.

## Content
* [Setup](#setup)
* [Usage](#usage)
* [Configuration](#configuration)
  * [PHP_CodeSniffer](#php_codesniffer-phpcs-phpcbf) (Codestyle)
  * [php-cs-fixer](#php-cs-fixer) (Codestyle)
  * [phpstan](#phpstan) (Code Analyse)
    * [baseline generation](#baseline-generation)
    * [ignoring errors in code](#ignoring-errors-in-code)
    * [ignoring errors in config](#ignoring-errors-in-config)
  * [psalm](#psalm) (Code Analyse)
    * [baseline generation](#baseline-generation-1)
    * [ignoring errors in code](#ignoring-errors-in-code-1)
    * [ignoring errors in config](#ignoring-errors-in-config-1)

## Setup

1. Copy files into root directory of your project (where your composer.json is located.)
and execute `setup-codecheck.sh`

2. Possibly change config files for codecheck tools according to your needs.

3. Add parts from the `example.gitlab-ci.yml` to your projects gitlab-ci.yml.

## Usage

The setup bash-script added some composer scripts you can run.
- `composer check` executes phpstan and psalm
- `composer lint` executes php-cs-fixer (without modifying your code) and phpcs
- `composer checkall` executes _check_ and _lint_ at once
- `composer fix` executes php-cs-fixer and phpcbf to fix your code automatically when possible.

You can also execute the single tools by using their names as scriptname:
- `composer phpstan`
- `composer psalm`
- `composer phpcs` (runs PHP_CodeSniffer and only reports errors)
- `composer phpcbf` (runs PHP_CodeSniffer to fix errors)
- `composer phpcsfixer` (or `composer phpcsfixer -- --dry-run` to only check without changing your code)
- `composer phpinsight` (WIP)
- `composer phpunit` executes phpunit to run Unit-Tests of Packages

## Configuration

### PHP_CodeSniffer (phpcs / phpcbf)
Configurations for php_codesniffer is saved in the `phpcs.xml` file.

For more information visit https://github.com/squizlabs/PHP_CodeSniffer/wiki/Configuration-Options

### php-cs-fixer
Configuration for php-cs-fixer is saved in the `.php-cs-fixer.dist.php` file.

For more information visit https://github.com/FriendsOfPHP/PHP-CS-Fixer/blob/master/doc/config.rst

### phpstan
Configuration for phpstan is saved in the `phpstan.neon.dist` file.

For more information visit https://phpstan.org/user-guide/getting-started

 #### baseline generation
   - If you can't fix all errors of phpstan at once, you can create a baseline file with `composer phpstan -- --generate-baseline`
   - If you have no more errors, the baseline file will not be deleted or emptied `[ERROR] No errors were found during the analysis. Baseline could not be generated.` Just empty the baseline file
     
     or delete the baseline file and remove the include from the `phpstan.neon.dist` file.

 #### ignoring errors in code
   - by adding a comment in the code: (all types of comments work. `/** */`, `/* */`, `//`)
     - `/** @phpstan-ignore-next-line */` ignores the line following the comment
     - `echo $foo; /** @phpstan-ignore-line */` ignores the line with the comment

 #### ignoring errors in config
   ```yaml
   parameters:
	   ignoreErrors:
	    - '#Call to an undefined method [a-zA-Z0-9\\_]+::doFoo\(\)#'
	    -
	       message: '#Access to an undefined property [a-zA-Z0-9\\_]+::\$foo#'
	       path: some/dir/SomeFile.php # single path
	       paths: # multiple paths
	         - some/dir/*
	         - other/dir/*
	       count: 2 # optional
   ```

### psalm
Configuration for psalm is saved in the `psalm.xml` file.

For more information visit https://psalm.dev/docs/

 #### baseline generation
   - If you can't fix all errors of psalm at once, you can create a baseline file with `composer psalm -- --set-baseline=psalm-baseline.xml`
   - If you fixed some issues and you don't want to add other new errors to the existing baseline, use `composer psalm -- --update-baseline`

 #### ignoring errors in code
   - by adding a docblock in the code:
     ```php
     /**
     * @psalm-suppress all
     */
     ```
     instead of `@psalm-suppress all` every issue type can be used, like `@psalm-suppress InvalidReturnType`

 #### ignoring errors in config
   ```xml
   <issueHandlers>
      <InvalidReturnType>
        <errorLevel type="suppress">
          <directory name="some_bad_directory" /> <!-- all InvalidReturnType issues in this directory are suppressed -->
          <file name="some_bad_file.php" />  <!-- all InvalidReturnType issues in this file are suppressed -->
        </errorLevel>
      </InvalidReturnType>
   </issueHandlers>
   ```
   for more posibilities in ignoring errors by config, visit https://psalm.dev/docs/running_psalm/dealing_with_code_issues/

